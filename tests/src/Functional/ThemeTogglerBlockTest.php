<?php

namespace Drupal\Tests\bootstrap_theme_toggler\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests to verify that the Bootstrap Theme Toggler block
 * can be placed into various themes.
 *
 * @group bootstrap_theme_toggler
 */
class ThemeTogglerBlockTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  // protected $defaultTheme = 'bootstrap_barrio';
  protected $defaultTheme = 'olivero';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['system', 'user', 'block', 'bootstrap_theme_toggler'];

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Create administrative user.
    $admin_user = $this->drupalCreateUser([
      'access administration pages',
      'administer themes',
      'administer blocks',
      'administer site configuration',
      'view the administration theme',
    ], 'Michael', true);
    $this->drupalLogin($admin_user);
  }


  /**
   * Tests that the home page loads with a 200 response.
   */
  public function testLoad() {
    // $this->drupalGet('/');
    $this->drupalGet(Url::fromRoute('<front>'));
    // $this->assertSession()->addressEquals('/');
    $this->assertSession()->statusCodeEquals(200);
    // $this->assertSession()->addressEquals(Url::fromRoute('<front>'));
  }

  /**
   * Test that the Toggler block can be placed.
   * Checked by testing the toggler drop-down label text appears on page.
   */
  public function testPlaceTogglerBlock() {
    // $themesToTest = array('olivero', 'bootstrap_barrio');
    $themesToTest = array('olivero');
    \Drupal::service('theme_installer')->install($themesToTest);
    // $theme_settings = $this->config('system.theme')->getOriginal('default');
    $theme_settings = $this->config('system.theme');

    foreach ($themesToTest as $theme) {
      dump($theme);
      $theme_settings->set('default', $theme)->save();

      if ($theme == 'bootstrap_barrio'){
        // For bootstrap_barrio theme, bootstrap is NOT loaded by default
        // This will load a bootstrap library for visual verification.
        // However, the local test assertion result doesn't care if bootstrap
        // is loaded or not it will still pass.
        $this->drupalGet('admin/appearance/settings/bootstrap_barrio');
        $page = $this->getSession()->getPage();
        // Select the 'local' option from the 'Load Library' drop down button
        $page->selectFieldOption('bootstrap_barrio_source', 'bootstrap_barrio/bootstrap');
        // Select the 'CDN' option from the 'Load Library' drop down button
        // $page->selectFieldOption('bootstrap_barrio_source', 'bootstrap_barrio/bootstrap_cdn');
        $this->submitForm([], 'Save configuration');
      }
      // Go to the block layout page.
      $this->drupalGet('admin/structure/block/list/' . $theme);
      // Configure and place toggler block.
      $this->drupalPlaceBlock('theme_toggler', [
        // Not all themes have a 'top_header'
        // Use a common region, like 'content'
        'region' => 'content',
        'theme' => $theme,
        'label' => 'Bootstrap Theme Toggler',
        'label_display' => 'visible',
        'show_checkmark' => 'true',
        'show_icons' => 'true',
        'show_toggler_label' => 'true',
        'toggler_label' => 'test-code 67j8',
        'show_option_labels' => 'true',
        'custom_only' => 'false',
      ]);
      // Go back to the homepage
      $this->drupalGet(Url::fromRoute('<front>'));
      // Checking for the block 'label' can return a false success because the block
      // label will appear on the page even if the block is broken.
      // $this->assertSession()->pageTextContains("Bootstrap Theme Toggler");
      // So instead we check for the label on the toggler element itself
      $this->assertSession()->pageTextContains("test-code 67j8");
    }// END foreach loop

  }// END testPlaceTogglerBlock

}
