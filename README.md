# Bootstrap Theme Toggler

*For full instructions, please see the [Bootstrap Theme Toggler Drupal documentation](https://www.drupal.org/docs/extending-drupal/contributed-modules/contributed-module-documentation/bootstrap-theme-toggler-block).*
<br>

The Bootstrap Theme Toggler module provides a drop-down button, block, that can be used to switch the active color scheme. Placing this block on any region of the site will enable the usage of `light` and `dark` [color modes](https://getbootstrap.com/docs/5.3/customize/color-modes/), introduced in [bootstrap 5.3](https://getbootstrap.com/).

In addition to, or as a replacement of, the standard built-in `light`, `dark`, and `auto` themes, this module provides a means to attaching custom color modes as well.


## Requirements
This module works, out of the box, with any site implmenting bootstrap 5.3 color modes.

### Required
-  [bootstrap >5.3](https://getbootstrap.com/) (included with Barrio Bootstrap 5 Theme)
-  \* \*\*[popper.js]() is required to enable the drop-down effetct.
<br>**\*** Barrio Bootstrap 5 Theme comes with a bootstrap librarly bundled with popper.js placed inside your site's `web/vendor/twbs/` folder. To enable drop-down effect, copy the contained `bootstrap` folder, to your site's `web/libraries/` folder, and the change `Load Library` option, on the Bootstrap Barrio settings page to `Local`.
<br>**\*\*** Bootstrap 5 SASS - Starter Kit also includes `popper.js`, and installs it to the theme's `sass_theme\js` folder, however, it doesn't use it by default. Instead, it relies on the Drupal core `popper.js`. As of Drupal 10 popper is no longer included in Drupal core so we must add it manually. To include `popper`, add `popper.min.js` to the SASS theme's `sass_theme.libraries.yml` as such. The `popper` library must be included before `bootstrap`.
```yml
  global-styling:
    version: VERSION
    js: 
      js/popper.min.js: {weight: -49}
      js/bootstrap.min.js: { weight: -48 }
      js/barrio.js: {}
      js/custom.js: {}
    css:
      component:
        css/style.css: {}
    dependencies:
      - core/jquery
      - core/drupal
      # - core/popperjs
  ```
 
### Recommended
- [Barrio Bootstrap 5 Theme](https://www.drupal.org/project/bootstrap_barrio)
- [Bootstrap 5 - SASS Starter Kit](https://www.drupal.org/project/bootstrap_sass) based sub-theme


## Installation
Install as you would normally install a contributed Drupal module. For further
information, see: [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration
The drop-down button, and the enabled theme palettes, can be configured via the Theme Toggler block's, configuration form, accesible from the Drupal Block Layout page.
1. From the Drupal Admin interface, navigate to:
   - `Structure » Block Layout` (admin/structure/block)
2. Ensure you're on the desired theme's layout tab.
3. Find the placed `Theme Toggler` block,
4. Click ***configure***


## Changing Toggler Icons
The icons used in the theme toggler, drop-down menu are stored in:
 - `web\modules\custom\bootstrap_theme_toggler\svg`
 - To change them, edit the contain svg path, or replace with another svg given the same name.
 - The svg must be a `<symbol>` element with the `id="themeID"`.
 - The icon name associated with a give `themeID` should use the prefix `icon_`, such that the filename has the convention `icon_themeID.svg`.


## Custom Color Mode Themes
A SASS based theme can implement custo color modes that can be enabled/disabled by from the toggler configuration menu. A cusom color mode is created by defining a CSS theme,
  ```
  [data-bs-theme='themeID'] {
    ---Add Theme pallett CSS--- 
  }
  ```
  inside a file `theme_themeID.scss`, placed in the SASS theme's scss folder (theme_name\scss). Where `themeID` in the filename should match the id given to the theme.
