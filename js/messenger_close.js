
// Used to close drupal messenger messages added by Theme Toggler code.
window.onload = () => {
  document.getElementById('messenger-close').onclick = function() {
    this.parentNode.parentNode.parentNode.remove()
    return false;
  };
};

