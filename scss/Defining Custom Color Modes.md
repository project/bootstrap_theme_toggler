# Custom Color Mode Themes
This folder, `boostrap_theme_toggler/scss`, is the default location to house custom color mode themes that can be added to your site, and accessed via the theme toggler. These custom color modes are SASS based definitions and thus require that the currently set default theme is a bootstrap SASS based theme whose compilation pipeline is updated to inculde the `themes.scss` entry file in this folder.

***See [Bootstrap Color Modes documentation](https://getbootstrap.com/docs/5.3/customize/color-modes/) to ensure your color mode definitions, and site styling architecture is compatible.***

***To create a bootstrap barrio SASS based sub-theme, install the [Bootstrap 5 - SASS Starter Kit](https://www.drupal.org/project/bootstrap_sass) and run the script.***

## Toggler Color Mode Discovery
The Bootstrap Theme Toggler will look for a `gulpfile.js` file located in the default theme's root directory, as an indication the loaded theme is a Bootstrap Barrio SASS based theme. If it finds one, it will automatically load the custom color modes defined in `boostrap_theme_toggler/scss`.

### Change Custom Color Mode File Directory
If it's desired to have the custom color mode theme files located in another folder, say inside the theme directory, to facilitate the SASS pipeline, an alternate folder can be set, inside the Theme Toggler global configuration form.

### Load Custom Color Modes for Non-Bootstrap Barrio Starter Kit Themes
If the default theme is a boostrap color mode compatible theme, but does not utilize a `gulpfile.js` file in its root directory, check the `Add custom color modes` option in the global Theme Toggler configuration form.

## Defining a Custom Color Mode
A color mode is defined by an `scss` file that is named after the `data-bs-theme` attribute it contains, prefixed with `'_'`, resulting in `_themeId.scss`.

*example `data-bs-theme` template*
```
[data-bs-theme="themeId"] = {
  --bs-body-color: var(--bs-white);
  --bs-body-color-rgb: #{to-rgb($white)};
  --bs-body-bg: var(--bs-blue);
  --bs-body-bg-rgb: #{to-rgb($blue)};
  --bs-tertiary-bg: #{$blue-600};

  .dropdown-menu {
    --bs-dropdown-bg: #{mix($blue-500, $blue-600)};
    --bs-dropdown-link-active-bg: #{$blue-700};
  }

  .btn-secondary {
    --bs-btn-bg: #{mix($gray-600, $blue-400, .5)};
    --bs-btn-border-color: #{rgba($white, .25)};
    --bs-btn-hover-bg: #{darken(mix($gray-600, $blue-400, .5), 5%)};
    --bs-btn-hover-border-color: #{rgba($white, .25)};
    --bs-btn-active-bg: #{darken(mix($gray-600, $blue-400, .5), 10%)};
    --bs-btn-active-border-color: #{rgba($white, .5)};
    --bs-btn-focus-border-color: #{rgba($white, .5)};
    --bs-btn-focus-box-shadow: 0 0 0 .25rem rgba(255, 255, 255, .2);
  }
}
```

All `scss` files, in the searched Custom Color Mode folder, prefixed with an underscore, `'_'`, of the form `_themeId.scss`, will be added to the `Theme Toggler` drop-down list with the label `themeId`.

example: if theme file is called `_blue.scss` then the theme option `blue` will be added to the toggler below the default `auto`, `light`, & `dark`.

To prevent a color mode theme from being loaded, simply edit the `_themeId.scss` filename to eliminate the `'_'` prefix, as such `disabled_themeId.scss`.

## The Custom Color Mode Folders `themes.scss` File
To take effect with a SASS based theme, the custom color mode themes need to be incorporated into the theme's SASS pipeline. The intended approach is to `@impoart` the `themes.scss` to the theme's SASS pipeline and add the `bootstrap_theme_toggler/scss` directory to the SASS watch list (or alternate custom color mode theme folder defined in the Theme Toggler global config).

### @import Custom Color Mode files into `themes.scss`
Once a custom color mode has been created in the defined search folder, it will be detectable by the Theme Toggler and added to the drop-down list. However, to make its style definitions accessible the `_themeId.scss` must also be added to the import list contained inside `themes.scss`.

## Overriding Default Bootstrap 'Light' & 'Dark' Modes
The `Light` and `Dark` color modes are defined, natively inside the bootstrap library. As such they are not directly available for customization. However, the Bootstrap Theme Toggler module provides the `default_overrides.scss` file in which any part of these default color palettes can be overridden. To take effect, make sure that the custom color modes are injected into the SASS pipeline after the bootstrap definitions.